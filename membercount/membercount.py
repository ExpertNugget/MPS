import discord
from redbot.core import commands
import json, os

if os.path.exists(os.getcwd() + "/membercountchannel.json"):
    with open("./membercountchannel.json")as f:
        configData = json.load(f)
else:
    configTemplate = {"channelid": "<Channel ID Here>"}
    with open(os.getcwd() + "/membercountchannel.json", "w+") as f:
        json.dump(configTemplate, f)
channel = configData["channelid"]

class membercount(commands.Cog):
    """membercount"""

    def __init__(self, bot):
        self.bot = bot

    #command to set channel for member count
    @commands.command()
    async def setmembercountchannel(self, ctx, channel: discord.TextChannel):
        if channel:
                channel = channel.id
                await ctx.message.send(f"Membercount channel set to <#{channel.id}>")
    
    
    #automatically renames channel with member count
    @commands.Cog.listener()
    async def on_member_join(self, member):
        if channel is not None:
            await channel.edit(name=f"Members: {member.guild.member_count}")



