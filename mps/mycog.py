import discord
from redbot.core import commands

class MPS(commands.Cog):
    """My custom cog"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def common(self, ctx):
        embed = discord.Embed()
        embed.add_field(name = 'Extra cmds', value= ";uptime Total time bot has been up.\n;sticky Pins messages to bottom of channels.\n;unsticky unPins messages.", inline = False)

        embed.set_author(name='Common Commands')

        await ctx.send(embed=embed)
