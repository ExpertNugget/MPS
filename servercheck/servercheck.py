from dis import dis
import discord
from mcstatus import MinecraftServer
from redbot.core import commands

class servercheck(commands.Cog):

    """Commands for checking the status of the server"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command(description='Gets information about the MPS MC server.', brief='Gets information about the MPS MC server.')
    async def mpsmc(self, ctx):
        """Displays server status of MPSMC."""
        try:
            server = MinecraftServer.lookup("167.114.15.118:25565")
            status = server.status()
            query = server.query()
            if status.players.online == 0:
                embed = discord.Embed(
                    title=(
                        'MPSMC is currently Online'
                        '\nNo players online. 😢'
                    )
                )
                embed.set_author(name='MPSMC Status')

                await ctx.send(embed=embed)
                
            else:
                embed = discord.Embed(
                title=('MPSMC is currently Online'
                        f'\n{status.players.online} players online'
                        '\nPlayer List:'
                        ),
                description=(f'{", ".join(query.players.names)}'))
                embed.set_author(name='MPSMC Status')
                embed.set_footer(text='End of list')

                await ctx.send(embed=embed)

        except:
            embed = discord.Embed(
            title=('MPSMC is Offline'),
            description=('Yell at <@166311283744964608>!')
            )
            await ctx.send(embed=embed)
    
    @commands.command(description='Gets information a server.(Note that Sub-Domains do not work and that servers need query enabled for this to work.)', brief='Gets information about a server')
    async def ip(self, ctx, ip, port=None):
        if port is None:
            try:
                server = MinecraftServer.lookup(f'{ip}:25565')
                status = server.status()
                query = server.query()

                if status.players.online == 0:
                    embed = discord.Embed(
                        title=(
                            'Server is currently Online'
                            '\nNo players online. 😢'
                        ))
                    embed.set_author(name='Server Status')

                    await ctx.send(embed=embed)
                elif status.players.online > 0:
                    embed = discord.Embed(
                        title=('Server is currently Online'
                                f'\n{status.players.online} players online'
                                '\nPlayer List:'
                                ),
                        description=(f'{", ".join(query.players.names)}'))
                    embed.set_author(name='Server Status')
                    embed.set_footer(text='End of list')

                    await ctx.send(embed=embed)
                else:
                    ctx.reply('Counldn\'t find a server, try adding the port.')
            except:
                ctx.reply('Counldn\'t find a server, try adding the port.')
        
        elif port is not None:
            try:
                server = MinecraftServer.lookup(f'{ip}:{port}')
                status = server.status()
                query = server.query()

                if status.players.online == 0:
                    embed = discord.Embed(
                        title=(
                            'Server is currently Online'
                            '\nNo players online. 😢'
                        )
                    )
                    embed.set_author(name='Server Status')

                    await ctx.send(embed=embed)
                else:
                    embed = discord.Embed(
                        title=('Server is currently Online'
                                f'\n{status.players.online} players online'
                                '\nPlayer List:'
                                ),
                        description=(f'{", ".join(query.players.names)}'))
                    embed.set_author(name='Server Status')
                    embed.set_footer(text='End of list')

                    await ctx.send(embed=embed)      
            except:
                await ctx.reply('failed to find server. Please use ;contact or make an issue on github')
        else:
            await ctx.reply('failed to find server. Please use ;contact or make an issue on github')
