import discord
from redbot.core import commands

class profile(commands.Cog):
        def __init__(self, bot):
            self.bot = bot
        
        @commands.command(description='Profile command to check yours or anothers profile.', brief='Profile command to check yours or anothers profile.')
        async def profile(self, ctx, user: discord.Member = None):            
            if user is None:
                user = ctx.author
                embed = discord.Embed(
                    title=f'{user}',
                    description=(f'Custom descriptions coming soon!'),
                    colour=discord.Colour.blue()
                )
                embed.add_field(name='Joined at', value=user.joined_at.strftime("%d/%m/%Y"), inline=True)
                embed.add_field(name='Created at', value=user.created_at.strftime("%d/%m/%Y"), inline=True)

                embed.set_thumbnail(url=user.avatar_url)
                embed.set_footer(text=f'Requested by {ctx.author}')
                
                await ctx.send(embed=embed)
            else:
                embed = discord.Embed(
                    title=f'{user}',
                    description=(f'Custom descriptions coming soon!'),
                    colour=discord.Colour.blue
                )
                embed.add_field(name='Joined at', value=user.joined_at.strftime("%d/%m/%Y"), inline=True)
                embed.add_field(name='Created at', value=user.created_at.strftime("%d/%m/%Y"), inline=True)

                embed.set_thumbnail(url=user.avatar_url)
                embed.set_footer(text=f'Requested by {ctx.author}')
                
                await ctx.send(embed=embed)

        @commands.command(description='Add or edit your profile description.', brief='Add or edit your profile description.')
        async def edit(self, ctx, *, description: str):
            await ctx.send(f'{description}\nThis is a TMP response.')