import discord

from discord.ext import commands
from redbot.core import commands

class morefornugget(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.has_permissions(administrator=True)
    async def say(self, ctx, *, message):
            if not message:
                await ctx.send("Please specify a message to send.")
                return
            await ctx.message.delete()
            await ctx.send(message)

    @commands.command()
    @commands.has_permissions(administrator=True)
    async def staffannounce(self, ctx, *, message):
            if not message:
                await ctx.send("Please specify the announcement message.")
                return
            await ctx.message.delete()
            embed=discord.Embed(title="Attention, Staff!", description=message, color=0x2ecc71)
            await ctx.send(embed=embed)


    @commands.command()
    @commands.has_permissions(administrator=True)
    async def announce(self, ctx, *, message):
            if not message:
                await ctx.send("Please specify the announcement message.")
                return
            await ctx.message.delete()
            embed=discord.Embed(title="Announcement!", description=message, color=0x2ecc71)
            await ctx.send(embed=embed)

    @commands.command(aliases=['cl'])
    @commands.has_permissions(administrator=True)
    async def changelog(self, ctx, *, message):
            if not message:
                await ctx.send("Please enter a change.")
                return
            await ctx.message.delete()
            embed=discord.Embed(title="Changes Made:", description=message, color=0x2ecc71)
            embed.set_footer(text='Check time posted for change time.')
            await ctx.send(embed=embed)

    @commands.command(description='Recommended format: ?category **Category** \n Info here.')
    @commands.has_permissions(administrator=True)
    async def category(self, ctx, *, message):
            if not message:
                await ctx.send("Please enter a message.")
                return
            await ctx.message.delete()
            embed=discord.Embed(description=message, color=0x2ecc71)
            embed.set_footer(text='Contact staff if you need any help.')
            embed.set_author(name='MPS Category Info', icon_url='https://media.discordapp.net/attachments/859710530884337684/876045070351097856/mps_512x512.png')
            await ctx.send(embed=embed)

    @commands.command(aliases=['topic'])
    async def revive(self, ctx, *, message):
            if not message:
                await ctx.send("Please enter a topic.")
                return
            await ctx.message.delete()
            embed=discord.Embed(title="Chat Revival!", description=message, color=0x2ecc71)
            embed.set_footer(text='What is so intresting about this footer?')
            await ctx.send(embed=embed)
            await ctx.send('<@&927689122901016607>')

def setup(bot):
    bot.add_cog(morefornugget(bot))